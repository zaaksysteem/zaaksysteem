BEGIN;

  CREATE OR REPLACE FUNCTION get_date_progress_from_case(
	IN completion_date timestamp without time zone,
	IN target_completion_date timestamp without time zone,
	IN registration_date timestamp without time zone,
    IN case_status text,
    OUT percentage numeric
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    completion_date_local timestamp with time zone;
    completion_epoch bigint;
    start_epoch bigint;
    target_epoch bigint;

    current_difference bigint;
    max_difference bigint;
  BEGIN

    percentage := Null;
    IF case_status = 'stalled'
    THEN
      RETURN;
    END IF;

    IF completion_date IS NOT NULL
    THEN
      completion_date_local := (completion_date)::timestamp without time zone;
    ELSE
      SELECT INTO completion_date_local NOW()::timestamp without time zone;
    END IF;

    SELECT INTO completion_epoch date_part('epoch', completion_date_local);
    SELECT INTO start_epoch date_part('epoch', registration_date::timestamp without time zone);

    current_difference := completion_epoch - start_epoch;

    SELECT INTO target_epoch date_part('epoch', target_completion_date::timestamp without time zone);

    max_difference := target_epoch - start_epoch;
    max_difference := GREATEST(max_difference, 1);

    percentage := ROUND(100 * ( current_difference::numeric / max_difference::numeric ));

    RETURN;
  END;
  $$;

COMMIT;
