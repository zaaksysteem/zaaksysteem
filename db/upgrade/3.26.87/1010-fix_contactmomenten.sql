BEGIN;

UPDATE contactmoment SET subject_id = regexp_replace(subject_id, '^betrokkene-(bedrijf|natuurlijk_persoon)-betrokkene-(bedrijf|natuurlijk_persoon)-', 'betrokkene-\1-');

COMMIT;