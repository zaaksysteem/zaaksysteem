BEGIN;

    ALTER TABLE bibliotheek_sjablonen ALTER filestore_id DROP NOT NULL;

    /*  In case you need to regenerate your schema or when your vagrant doesn't want to work
        uncomment these lines

        delete from bibliotheek_sjablonen_magic_string where bibliotheek_sjablonen_id in (
            select id from bibliotheek_sjablonen where filestore_id is null and interface_id is null
        );

        delete from zaaktype_sjablonen where bibliotheek_sjablonen_id in (
            select id from bibliotheek_sjablonen where filestore_id is null and interface_id is null
        );

        delete from bibliotheek_sjablonen where filestore_id is null and interface_id is null;
    */

    ALTER TABLE bibliotheek_sjablonen ADD CONSTRAINT filestore_or_interface CHECK ( filestore_id IS NOT NULL OR interface_id IS NOT NULL );

COMMIT;

