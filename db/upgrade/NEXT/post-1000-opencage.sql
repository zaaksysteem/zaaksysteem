BEGIN;

    UPDATE interface SET date_deleted = CURRENT_TIMESTAMP AT TIME ZONE 'UTC' WHERE module = 'opencage' AND date_deleted IS NULL;

COMMIT;
