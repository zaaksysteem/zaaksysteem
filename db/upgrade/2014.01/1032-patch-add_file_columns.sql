BEGIN;

ALTER TABLE file ADD COLUMN active_version BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE file ADD COLUMN is_duplicate_of INTEGER REFERENCES file(id) ON DELETE SET NULL;

UPDATE file orig set active_version = true where orig.date_deleted is null and orig.root_file_id is null and orig.destroyed = false and (select count(id) from file dup where dup.root_file_id = orig.id) < 1;

UPDATE file set active_version = true where id in (select distinct on (root_file_id) id from file order by root_file_id, version desc) and date_deleted is null and destroyed = false;

WITH dupquery AS (
    SELECT
        DISTINCT ON (orig_file.id) orig_file.id as orig_id, orig_file.case_id,dup_file.id AS dup_id,orig_file.name
    FROM
        file orig_file,
        (
            SELECT
                id,case_id,name,extension
            FROM
                file
            WHERE
                date_deleted is null AND accepted = true AND is_duplicate_name = false AND date_deleted is null
        ) dup_file
    WHERE
        orig_file.case_id = dup_file.case_id and orig_file.is_duplicate_name = true and orig_file.name=dup_file.name and orig_file.extension = dup_file.extension and orig_file.date_deleted is null and orig_file.accepted=false and orig_file.accepted = false
) UPDATE
    file
SET
    is_duplicate_of=uq.dup_id
FROM
    (select * from dupquery) as uq
WHERE
    id=uq.orig_id;

COMMIT;