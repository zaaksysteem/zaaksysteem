BEGIN;

  DROP TABLE IF EXISTS contact_relationship;

  CREATE TABLE contact_relationship (
    id SERIAL PRIMARY KEY,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    contact int NOT NULL,
    contact_uuid UUID NOT NULL,
    contact_type TEXT NOT NULL,
    relation int NOT NULL,
    relation_uuid UUID NOT NULL,
    relation_type TEXT NOT NULL
  );

  /* Create a unique index that prevents the following:

  INSERT INTO contact_relationship (contact, contact_type, relation, relation_type)
  VALUES
    (
      1, 'foo',
      2, 'bar'
  );

  INSERT INTO contact_relationship (contact, contact_type, relation, relation_type)
  VALUES
    (
      2, 'bar',
      1, 'foo'
  );

  */

  CREATE UNIQUE INDEX contact_relationship_unique_link_idx ON contact_relationship USING btree (
      LEAST(
        (contact::int || contact_type::text),
        (relation::int || relation_type::text)
      ),
      GREATEST(
        (contact::int || contact_type::text),
        (relation::int || relation_type::text)
      )
  );

  CREATE INDEX contact_relationship_uuid_idx ON contact_relationship(uuid);

COMMIT;
