BEGIN;

    ALTER TABLE interface ADD internal_config JSONB NOT NULL DEFAULT '{}'::JSONB;

COMMIT;