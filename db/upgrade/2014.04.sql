BEGIN;

alter table zaak add stalled_until timestamp;
alter table zaaktype_authorisation add confidential boolean default 'f' not null;
create type confidentiality as enum ('public', 'internal', 'confidential');
alter table zaak add confidentiality confidentiality not null default 'public';

CREATE EXTENSION IF NOT EXISTS "hstore";

ALTER TABLE zaak ADD hstore_properties HSTORE;

alter table zaaktype_kenmerken drop besluit;
alter table zaaktype_definitie drop custom_webform;
drop table parkeergebied_kosten;
alter table zaak drop confidentiality;
alter table zaak add confidentiality varchar(255) default 'public' check (confidentiality in ('public', 'internal', 'confidential'));
alter table logging alter onderwerp type text;

alter table logging alter onderwerp type text;

DROP TABLE IF EXISTS notitie;
DROP TABLE IF EXISTS jobs;


-- object_data, containing all the object data
-- Features:
-- + Hstore: for fast search filtering
-- + object_id: for constraint bound creating serials for objects which
--   need an id (like case)

DROP TABLE IF EXISTS object_data;

CREATE TABLE object_data (
    uuid                            UUID UNIQUE PRIMARY KEY DEFAULT uuid_generate_v4(),
    object_id                       INTEGER,
    object_class                    TEXT NOT NULL CHECK(
        (object_class)::TEXT ~ '^case$'::TEXT
    ),
    properties                      TEXT NOT NULL DEFAULT '{}',
    index_hstore                    HSTORE,
    date_created                    timestamp without time zone,
    date_modified                   timestamp without time zone,
    UNIQUE(object_class, object_id)
);

-- select properties me,array(
--     select distinct properties from object_data, object_relationships where
--       (
--         object_relationships.object1_uuid = me.uuid OR object_relationships.object2_uuid = me.uuid
--       ) AND 
--       (
--         object_data.uuid = object_relationships.object1_uuid OR
--         object_data.uuid = object_relationships.object2_uuid 
--       ) AND object_data.uuid != me.uuid
--   ) as related_objects from object_data me;

DROP TABLE IF EXISTS object_relationships;

CREATE TABLE object_relationships (
    uuid                            UUID UNIQUE PRIMARY KEY DEFAULT uuid_generate_v4(),
    object1_uuid                    UUID NOT NULL,
    object2_uuid                    UUID NOT NULL
);

drop table if exists file_annotation;

create table file_annotation (
    id uuid PRIMARY KEY,
    file_id integer not null,
    subject varchar(255) not null,
    properties text not null default '{}',
    created timestamp not null default NOW(),
    modified timestamp
);

ALTER TABLE ONLY file_annotation
    ADD CONSTRAINT file_annotation_file_id_fkey FOREIGN KEY (file_id) REFERENCES file(id);



-- This is the new index that makes object search lightning fast:
DROP INDEX IF EXISTS object_data_hstore_idx;
CREATE INDEX object_data_hstore_idx ON object_data USING GIN(index_hstore);

-- These indexes make "ORDER BY" faster. Also range searches.
DROP INDEX IF EXISTS object_data_completion_idx;
DROP INDEX IF EXISTS object_data_assignee_idx;
DROP INDEX IF EXISTS object_data_casetype_description_idx;
DROP INDEX IF EXISTS object_data_recipient_name_idx;
DROP INDEX IF EXISTS object_data_regdate_idx;
DROP INDEX IF EXISTS object_data_target_date_idx;
DROP INDEX IF EXISTS object_data_case_phase_idx;
DROP INDEX IF EXISTS object_data_case_number_idx;
DROP INDEX IF EXISTS object_data_casetype_name_idx;

CREATE OR REPLACE FUNCTION hstore_to_timestamp(IN date_field VARCHAR)
   RETURNS TIMESTAMP
   LANGUAGE SQL
   IMMUTABLE
AS
$body$
  SELECT to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS')::TIMESTAMP;
$body$;


CREATE INDEX object_data_completion_idx
    ON object_data((index_hstore->'case.date_of_completion'));

CREATE INDEX object_data_assignee_idx
    ON object_data((index_hstore->'case.assignee'));

CREATE INDEX object_data_casetype_description_idx
    ON object_data((index_hstore->'casetype.description'));

CREATE INDEX object_data_recipient_name_idx
    ON object_data((index_hstore->'recipient.full_name'));

CREATE INDEX object_data_regdate_idx
    ON object_data(hstore_to_timestamp(index_hstore->'case.date_of_registration'));

CREATE INDEX object_data_target_date_idx
    ON object_data(hstore_to_timestamp(index_hstore->'case.date_target'));

CREATE INDEX object_data_case_phase_idx
    ON object_data((index_hstore->'case.phase'));

CREATE INDEX object_data_case_number_idx
    ON object_data((index_hstore->'case.number'));

CREATE INDEX object_data_casetype_name_idx
    ON object_data((index_hstore->'casetype.name'));

-- used for inavigator import, see if we already have this checklist item in our casetype
alter table zaaktype_status_checklist_item add external_reference text;

ALTER TABLE object_data ADD COLUMN text_vector tsvector;

CREATE INDEX object_text_vector_idx ON object_data USING gin(text_vector);


ALTER TABLE object_data DROP CONSTRAINT IF EXISTS object_data_object_class_check;
ALTER TABLE object_data ADD CONSTRAINT object_data_object_class_check CHECK(
    object_class ~ '^(case|saved_search)$'
);


ALTER TABLE object_relationships ADD type1 TEXT NOT NULL;
ALTER TABLE object_relationships ADD type2 TEXT NOT NULL;

ALTER TABLE object_relationships
    ADD FOREIGN KEY (object1_uuid) REFERENCES object_data(uuid);
ALTER TABLE object_relationships
    ADD FOREIGN KEY (object2_uuid) REFERENCES object_data(uuid);


    ALTER TABLE zaaktype_definitie DROP IF EXISTS iv3_categorie;
    ALTER TABLE zaaktype_definitie DROP IF EXISTS besluittype;

alter table zaaktype_resultaten add external_reference text;

ALTER TABLE bag_nummeraanduiding ADD COLUMN gps_lat_lon point;
ALTER TABLE bag_openbareruimte ADD COLUMN gps_lat_lon point;

alter table logging alter event_type type text;

CREATE TABLE object_acl_entry (
    uuid UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    object_uuid UUID NOT NULL,
    entity_type TEXT NOT NULL,
    entity_id TEXT NOT NULL,
    permission TEXT NOT NULL
);

ALTER TABLE ONLY object_acl_entry ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid);

CREATE INDEX object_acl_entry_permission_idx ON object_acl_entry(permission);
CREATE INDEX object_acl_object_uuid_idx ON object_acl_entry(object_uuid);
CREATE INDEX object_acl_entity_idx ON object_acl_entry(entity_type, entity_id);


ALTER TABLE bedrijf ADD COLUMN vestigingsnummer BIGINT;


ALTER TABLE object_data DROP CONSTRAINT IF EXISTS object_data_object_class_check;

ALTER TABLE object_data ADD COLUMN class_uuid UUID REFERENCES object_data(uuid);


ALTER TABLE gm_bedrijf ADD COLUMN vestigingsnummer BIGINT;


ALTER TABLE object_acl_entry RENAME COLUMN permission TO capability;
ALTER TABLE object_acl_entry ADD COLUMN verdict TEXT;
ALTER TABLE object_acl_entry ADD CONSTRAINT object_acl_entry_verdict_check CHECK(
    verdict ~ '^(permission|proscription)$'
);


ALTER TABLE object_relationships
    DROP CONSTRAINT IF EXISTS object_relationships_object1_uuid_fkey,
    DROP CONSTRAINT IF EXISTS object_relationships_object2_uuid_fkey,
    ADD CONSTRAINT object_relationships_object1_uuid_fkey FOREIGN KEY (object1_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE,
    ADD CONSTRAINT object_relationships_object2_uuid_fkey FOREIGN KEY (object2_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;

ALTER TABLE object_acl_entry
    DROP CONSTRAINT IF EXISTS object_data_fkey,
    ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;

alter table zaaktype_definitie alter grondslag type text;

UPDATE zaaktype_node SET properties = '{}' WHERE properties IS NULL;
ALTER TABLE zaaktype_node ALTER COLUMN properties SET DEFAULT '{}';
ALTER TABLE zaaktype_node ALTER COLUMN properties SET NOT NULL;

COMMIT;
