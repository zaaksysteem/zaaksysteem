BEGIN;
  ALTER TABLE zaak_meta ADD COLUMN unaccepted_attribute_update_count int NOT NULL default 0;
COMMIT;
