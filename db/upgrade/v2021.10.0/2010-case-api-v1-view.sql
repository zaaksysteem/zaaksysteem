BEGIN;

  DROP INDEX IF EXISTS bibliotheek_kenmerken_value_type_idx;
  CREATE INDEX bibliotheek_kenmerken_value_type_idx ON bibliotheek_kenmerken(value_type);

  DROP VIEW IF EXISTS case_attributes CASCADE;

  CREATE VIEW case_attributes AS
    SELECT
      z.id AS case_id,
      COALESCE(zk.value, '{}') as value,
      bk.magic_string as magic_string,
      bk.id as library_id,
      bk.value_type as value_type
    FROM zaak z
    JOIN
      zaaktype_kenmerken ztk
    ON
      (z.zaaktype_node_id = ztk.zaaktype_node_id)
    JOIN
      bibliotheek_kenmerken bk
    ON
      (bk.id = ztk.bibliotheek_kenmerken_id AND bk.value_type NOT IN ('file', 'appointment'))
    LEFT JOIN
      zaak_kenmerk zk
    ON
      (z.id = zk.zaak_id AND zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id)
    GROUP BY 1,2,3,4,5
  ;

  DROP VIEW IF EXISTS case_attributes_appointments CASCADE;

  CREATE VIEW case_attributes_appointments AS
    SELECT
      z.id AS case_id,
      COALESCE(od.properties, '{}') as value,
      bk.magic_string as magic_string,
      bk.id as library_id,
      od.uuid as reference
    FROM zaak z
    JOIN
      zaaktype_kenmerken ztk
    ON
      (z.zaaktype_node_id = ztk.zaaktype_node_id)
    JOIN
      bibliotheek_kenmerken bk
    ON
      (bk.id = ztk.bibliotheek_kenmerken_id AND bk.value_type = 'appointment')
    LEFT JOIN
      zaak_kenmerk zk
    ON
      (z.id = zk.zaak_id AND zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id)
    LEFT JOIN
      object_data od
    ON
      zk.value[1]::uuid = od.uuid
    GROUP BY 1,2,3,4,5
  ;

  CREATE OR REPLACE FUNCTION appointment_attribute_value_to_jsonb(
    IN value text,
    IN reference uuid,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    appointment jsonb;
    preview text;
    start_time timestamp with time zone;
    end_time timestamp with time zone;
  BEGIN

      IF value = '{}'
      THEN
        RETURN;
      END IF;

      appointment := (value)::jsonb->'values';

      start_time := (appointment->'start_time'->'value'->>'__DateTime__')::timestamp with time zone;
      end_time := (appointment->'end_time'->'value'->>'__DateTime__')::timestamp with time zone;

      preview := concat(
        to_char(timezone('Europe/Amsterdam', start_time), 'YYYY-MM-DD HH:MI'),
        ' - ',
        to_char(timezone('Europe/Amsterdam', end_time), 'YYYY-MM-DD HH:MI')
      );

      SELECT INTO value_json jsonb_build_object(
        'type', 'appointment',
        'reference', reference,
        'preview', preview,
        'instance', jsonb_build_object(
          'date_created', (appointment->'date_created'->'value'->>'__DateTime__')::timestamp with time zone,
          'date_modified', (appointment->'date_modified'->'value'->>'__DateTime__')::timestamp with time zone,
          'start_time', start_time,
          'end_time', end_time,
          'plugin_type', appointment->'plugin_type'->>'value',
          'plugin_data', appointment->'plugin_data'->'value'
        )
      );
      SELECT INTO value_json to_jsonb(ARRAY[value_json]);
      RETURN;
  END;
  $$;

  CREATE VIEW case_attributes_v1 AS
    SELECT case_id, magic_string, library_id, attribute_value_to_jsonb(value, value_type) as value FROM case_attributes
  UNION ALL
    SELECT case_id, magic_string, library_id, appointment_attribute_value_to_jsonb(value, reference) as value FROM case_attributes_appointments
  UNION ALL
    SELECT case_id, magic_string, library_id, attribute_value_to_jsonb(value, 'file') from case_documents
  ;

  DROP VIEW IF EXISTS case_v1;

  CREATE VIEW case_v1 AS
  SELECT

    z.id AS number,
    z.uuid AS id,
    z.pid AS number_parent,
    z.number_master AS number_master,
    z.vervolg_van AS number_previous,

    z.onderwerp AS subject,
    z.onderwerp_extern AS subject_external,

    z.status AS status,

    z.created AS date_created,
    z.last_modified AS date_modified,
    z.vernietigingsdatum AS date_destruction,
    z.afhandeldatum AS date_of_completion,
    z.registratiedatum AS date_of_registration,
    z.streefafhandeldatum AS date_target,

    z.payment_status AS payment_status,
    z.payment_amount AS price,

    z.contactkanaal AS channel_of_contact,
    z.archival_state AS archival_state,

    CASE WHEN z.status = 'stalled' THEN
      zm.stalled_since
    ELSE
      NULL
    END AS stalled_since,

    CASE WHEN z.status = 'stalled' THEN
      z.stalled_until
    ELSE
      NULL
    END AS stalled_until,

    zm.current_deadline AS current_deadline,
    zm.deadline_timeline AS deadline_timeline,

    jsonb_object_agg(ca.magic_string, ca.value::jsonb) AS attributes,

    ztr.id AS result_id,
    ztr.resultaat as result,
    ztr.selectielijst as active_selection_list,

    CASE WHEN (ztr.id IS NOT NULL) THEN
      json_build_object(
        'reference', NULL,
        'type', 'case/result',
        'preview', CASE WHEN ztr.label IS NOT NULL THEN
          ztr.label
        ELSE
          ztr.resultaat
        END,
        'instance', json_build_object(
          'date_created', ztr.created,
          'date_modified', ztr.last_modified,
          'archival_type', ztr.archiefnominatie,
          'dossier_type', ztr.dossiertype,
          'name', CASE WHEN ztr.label IS NOT NULL THEN
            ztr.label
          ELSE
            ztr.resultaat
          END,
          'result', ztr.resultaat,
          'retention_period', ztr.bewaartermijn,
          'selection_list', CASE WHEN ztr.selectielijst = '' THEN
                              NULL
                            ELSE
                              ztr.selectielijst
                            END,
          'selection_list_start', ztr.selectielijst_brondatum,
          'selection_list_end', ztr.selectielijst_einddatum
        )
      )
    ELSE
      NULL
    END AS outcome,

    json_build_object(
      'preview', ct_ref.title,
      'reference', ct_ref.uuid,
      'instance', json_build_object(
        'version', ct_ref.version,
        'name', ct_ref.title
      ),
      'type', 'casetype'
    ) AS casetype,

    json_build_object(
      'preview', gr.name || ', ' || role.name,
      'reference', NULL,
      'type', 'case/route',
      'instance', json_build_object(
        'date_modified', NOW(),
        'date_created', NOW(),
        'group', gr.json,
        'role', role.json
      )
    ) AS route,

    CASE WHEN z.status = 'stalled' THEN
      zm.opschorten
    ELSE
      NULL
    END AS suspension_rationale,

    CASE WHEN z.status = 'resolved' THEN
      zm.afhandeling
    ELSE
      NULL
    END AS premature_completion_rationale,

    zts.fase::text AS phase,

    json_build_object(
      'preview', zts.fase,
      'reference', null,
      'type', 'case/milestone',
      'instance', json_build_object(
        'date_created', NOW(),
        'date_modified', NOW(),
          'phase_label',
            CASE WHEN zts.id IS NOT NULL THEN
              zts.naam
            ELSE
              zts_end.naam
            END,
          'phase_sequence_number',
            CASE WHEN zts.id IS NOT NULL THEN
              zts.status
            ELSE
              zts_end.status
            END,

        'milestone_label', zts_previous.naam,
        'milestone_sequence_number', zts_previous.status,
        'last_sequence', zts_end.status
      )
    ) as milestone,

    json_build_object(
      'type', 'set',
      'instance', json_build_object(
        'rows', COALESCE(crp.relationship, '[]'::jsonb)
      )
    ) AS relations,

    json_build_object(
      'parent', z.pid,
      'continuation', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(continuation.relationship, '[]'::jsonb)
        )
      ),
      'child', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(children.relationship, '[]'::jsonb)
        )
      ),
      'plain', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(crp.relationship, '[]'::jsonb)
        )
      )
    ) AS case_relationships,

    -- Subject API/v1 shit, mind boggling
    requestor.subject::jsonb AS requestor,
    assignee.subject::jsonb AS assignee,
    coordinator.subject::jsonb AS coordinator,

    -- static values
    'Dossier' AS aggregation_scope,

    -- Not available via api/v1
    null AS case_location,
    null AS correspondence_location

  FROM zaak z

  JOIN zaak_meta zm
  ON zm.zaak_id = z.id

  JOIN case_attributes_v1 ca
  ON  ca.case_id = z.id

  JOIN casetype_v1_reference ct_ref
  ON z.zaaktype_node_id = ct_ref.casetype_node_id

  LEFT JOIN zaaktype_resultaten ztr
  ON z.resultaat_id = ztr.id

  JOIN group_v1_view gr
  ON (z.route_ou = gr.id)

  JOIN role_v1_view role
  ON (z.route_role = role.id)

  LEFT JOIN zaaktype_status zts
  ON ( z.zaaktype_node_id = zts.zaaktype_node_id AND zts.status = z.milestone + 1)

  LEFT JOIN zaaktype_status zts_previous
  ON ( z.zaaktype_node_id = zts_previous.zaaktype_node_id AND zts_previous.status = z.milestone)

  JOIN casetype_end_status zts_end
  ON z.zaaktype_node_id = zts_end.zaaktype_node_id

  LEFT JOIN case_relationship_json_view crp
  ON ( z.id = crp.case_id and crp.type = 'plain')

  LEFT JOIN case_relationship_json_view continuation
  ON ( z.id = continuation.case_id and continuation.type = 'initiator')

  LEFT JOIN case_relationship_json_view children
  ON ( z.id = children.case_id and children.type = 'parent')

  JOIN case_v1_subjects requestor
  ON (z.id = requestor.case_id and requestor.type = 'requestor')

  LEFT JOIN case_v1_subjects coordinator
  ON (z.id = coordinator.case_id and coordinator.type = 'coordinator')

  LEFT JOIN case_v1_subjects assignee
  ON (z.id = assignee.case_id and assignee.type = 'assignee')

  WHERE z.deleted IS NULL

  GROUP BY
    z.id,
    zm.stalled_since,
    zm.current_deadline,
    zm.deadline_timeline,
    zm.opschorten,
    zm.afhandeling,
    ct_ref.title,
    ct_ref.uuid,
    ct_ref.version,
    role.name,
    role.json::jsonb,
    gr.name,
    gr.json::jsonb,
    ztr.id,
    zts.id,
    zts_previous.id,
    zts_end.status,
    zts_end.naam,
    crp.relationship::jsonb,
    continuation.relationship::jsonb,
    children.relationship::jsonb,
    requestor.subject::jsonb,
    coordinator.subject::jsonb,
    assignee.subject::jsonb
  ;

COMMIT;

