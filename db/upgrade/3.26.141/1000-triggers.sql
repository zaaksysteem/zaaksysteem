BEGIN;

    CREATE OR REPLACE FUNCTION insert_file_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
        BEGIN
            NEW.date_created = NOW() AT TIME ZONE 'UTC';
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;

    CREATE OR REPLACE FUNCTION update_file_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
        BEGIN
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;

    DROP TRIGGER IF EXISTS file_insert_timestamp_trigger ON file;
    CREATE TRIGGER file_insert_timestamp_trigger
        BEFORE INSERT ON file
        FOR EACH ROW
        EXECUTE PROCEDURE insert_file_timestamps();

    DROP TRIGGER IF EXISTS file_update_timestamp_trigger ON bibliotheek_notificaties;
    DROP TRIGGER IF EXISTS file_update_timestamp_trigger ON file;
    CREATE TRIGGER file_update_timestamp_trigger
        BEFORE UPDATE ON file
        FOR EACH ROW
        EXECUTE PROCEDURE update_file_timestamps();

    UPDATE file SET date_modified = date_created WHERE date_modified IS NULL;

COMMIT;
