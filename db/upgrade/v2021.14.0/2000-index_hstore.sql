BEGIN;

  ALTER TABLE zaak_meta ADD COLUMN index_hstore hstore;
  ALTER TABLE zaak_meta ADD COLUMN text_vector tsvector;

COMMIT;
