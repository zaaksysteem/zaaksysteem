BEGIN;

INSERT INTO queue (type, label, priority, metadata, data)
SELECT
  'store_object_label',
  'Add object label to object_data',
  950,
  '{"require_object_model":1, "disable_acl": 1, "target":"backend"}',
  json_build_object('object_uuid', od.uuid)
FROM
  object_data od
  JOIN object_data class_od ON class_od.uuid = od.class_uuid
WHERE
  class_od.object_class = 'type';

COMMIT;
