BEGIN;

ALTER TABLE object_data DROP CONSTRAINT IF EXISTS object_data_object_class_check;

ALTER TABLE object_data ADD COLUMN class_uuid UUID REFERENCES object_data(uuid);

COMMIT;
