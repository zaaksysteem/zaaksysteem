BEGIN;

DROP TABLE IF EXISTS dashboard;

CREATE TABLE dashboard (
	"id" integer GENERATED ALWAYS AS IDENTITY,
    "uuid" uuid UNIQUE,
	"widgets" jsonb NOT NULL DEFAULT '[]'::jsonb,
    CHECK (jsonb_typeof(widgets) = 'array'::text),
    FOREIGN KEY(uuid) REFERENCES subject(UUID) ON DELETE CASCADE
);


COMMIT;