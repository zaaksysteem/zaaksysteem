import {
    openPageAs
} from './../../../../../functions/common/navigate';
import {
    inputAttribute,
    inputAttributes
} from './../../../../../functions/common/input/caseAttribute';
import {
    isAdvancePossible
} from './../../../../../functions/intern/caseView/casePhase';

const form = $('.phase-form');

describe('when opening the case and not filling out the required attributes', () => {

    beforeAll(() => {

        openPageAs('admin', 171);

    });

    it('the advance button should be disabled', () => {

        expect(isAdvancePossible()).toBe(false);
        
    });
    
});

describe('when opening the case and filling out the required attributes', () => {

    beforeAll(() => {

        openPageAs('admin', 172);

        const testData = [
            'adres_dmv_postcode',
            'tekstveld',
            'enkelvoudige_keuze',
            'meervoudige_keuze',
            'datum',
            'document'
        ].map(attrName => {
            return { attr: form.$(`[data-name="verplichtheid_${attrName}"]`)}
        });

        inputAttributes(testData);

    });

    it('the advance button should be enabled', () => {

        expect(isAdvancePossible()).toBe(true);

    });

});

describe('when opening the case and hiding the required attributes', () => {

    beforeAll(() => {

        openPageAs('admin', 173);

        inputAttribute(form.$('[data-name="verplichtheid_velden_verbergen"]'), 1);

    });

    it('the advance button should be enabled', () => {

        expect(isAdvancePossible()).toBe(true);

    });

});

describe('when opening the case and hiding the group with required attributes', () => {

    beforeAll(() => {

        openPageAs('admin', 174);

        inputAttribute(form.$('[data-name="verplichtheid_velden_verbergen"]'), 1);

    });

    it('the advance button should be enabled', () => {

        expect(isAdvancePossible()).toBe(true);

    });

});
