import {
    openPageAs
} from './../../../../functions/common/navigate';
import {
    openTab
} from './../../../../functions/intern/caseView/caseNav';
import {
	uploadDocument,
	checkDocument
} from './../../../../functions/intern/caseView/caseDocuments';

describe('when opening a case, opening the document tab, and uploading a document', () => {

    beforeAll(() => {

        openPageAs('admin', 177);

        openTab('docs');

        uploadDocument();

    });

    it('the document should be present on the tab', () => {

        expect(checkDocument('text.txt')).toBe(true);

    });

});
