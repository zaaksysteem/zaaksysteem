import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    createContact
} from './../../../functions/intern/plusMenu';
import {
    search,
    exitUniversalSearch,
    countResults
} from './../../../functions/intern/universalSearch';

const contactsInfo = [
    {
        scenario: 'a citizen with a residence address',
        betrokkene_type: 'natuurlijk_persoon',
        'np-geslachtsnaam': 'citizenResidence'
    },
    {
        scenario: 'a citizen with a correspondence address',
        betrokkene_type: 'natuurlijk_persoon',
        'np-geslachtsnaam': 'citizenCorrespondence',
        briefadres: 'yes'
    },
    {
        scenario: 'a citizen with a foreign address',
        betrokkene_type: 'natuurlijk_persoon',
        'np-geslachtsnaam': 'citizenForeign',
        'np-landcode': 'Marokko'
    },
    {
        scenario: 'an organisation with an establishment address',
        betrokkene_type: 'bedrijf',
        handelsnaam: 'companyEstablishmentBasic'
    },
    {
        scenario: 'an organisation with a foreign address',
        betrokkene_type: 'bedrijf',
        handelsnaam: 'companyForeignBasic',
        'vestiging-landcode': 'Marokko'
    },
    {
        scenario: 'an organisation with an establishment correspondence address',
        betrokkene_type: 'bedrijf',
        handelsnaam: 'companyEstablishmentCorrespondence',
        'org-briefadres': true
    },
    {
        scenario: 'an organisation with a foreign correspondence address',
        betrokkene_type: 'bedrijf',
        handelsnaam: 'companyForeignCorrespondence',
        'org-briefadres': true,
        correspondentie_landcode: 'Marokko'
    }
];

describe('when opening the form to register a contact', () => {
    beforeAll(() => {
        openPageAs();
    });

    contactsInfo.forEach(contactInfo => {
        const { scenario, } = contactInfo;
        const searchValue = contactInfo['np-geslachtsnaam'] || contactInfo.handelsnaam;

        describe(`and registering ${scenario}`, () => {
            beforeAll(() => {
                createContact(contactInfo);
                search(searchValue);
            });
        
            it('there should be a snack with a link', () => {
                expect(countResults()).toEqual(1);
            });

            afterAll(() => {
                exitUniversalSearch();
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
