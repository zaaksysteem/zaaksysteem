# Installs zs-tools library from git repo

git "#{Chef::Config['file_cache_path']}/zs-tools-repo" do
    repository node['zaaksysteem']['tools']['repository']
    action :sync
    notifies :run, "bash[zs_tools_make_install]", :immediately
end

bash "zs_tools_make_install" do
    user "root"
    cwd "#{Chef::Config['file_cache_path']}/zs-tools-repo"
    code <<-EOS
        [[ -f Makefile ]] && make clean
        cpanm -i .
    EOS
    action :nothing
end
