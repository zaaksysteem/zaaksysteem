package TestFor::General::StUF::0301::Processor::BG0310;
use base 'ZSTest';

use TestSetup;
use Zaaksysteem::StUF::0301::Processor::BG0310;
use File::Spec::Functions qw(catfile);

use constant XML_PATH => [qw/t inc API StUF 0310 prs/];

=head1 NAME

TestFor::General::StUF::0301::Processor::BG0310 - StUF Koppeling processor: reading and writing XML

=head1 SYNOPSIS

    See USAGE tests

    ./zs_prove -v t/lib/TestFor/General/StUF/0301/Processor/BG0310.pm

=head1 DESCRIPTION

These tests prove the XML reading and writing interactions. It tests the little layer which transforms
raw XML into readable parameters and visa versa.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF XML.

=head2 processor_bg0310_usage_search_np

Searching for a person.

=cut

sub processor_bg0310_usage_search_np_raw : Tests {
    my $self        = shift;

    my $xml_tests = {

        'entiteittype'  => qr|<st:entiteittype>NPS</st:entiteittype>|,
        'zender.applicatie'  => qr|<st:applicatie>ZSNL</st:applicatie>|,
        'ontvanger.applicatie'  => qr|<st:applicatie>DDS</st:applicatie>|,
        'berichtcode'   => qr|<st:berichtcode>Lv01</st:berichtcode>|,
        'tijdstipbericht' => qr|<st:tijdstipBericht>\d{17}</st:tijdstipBericht>|,
        'referentienummer' => qr|<st:referentienummer>565487852</st:referentienummer>|,
    };

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );


    {   ### Primary search fields
        ### * Burgerservicenummer
        ### * voorvoegsel
        ### * geslachtsnaam (family_name)
        ### * geslacht
        ### * geboortedatum
        my $xml         = $processor->search_np(
            {
                personal_number     => '565898785',
                prefix              => 'De',
                family_name         => 'Fue',
                gender              => 'M',
                date_of_birth       => '19830405'
            }
        );

        like($xml, $xml_tests->{$_}, 'Found proper info in XML for "' . $_ . '"') for keys %$xml_tests;
        like($xml, qr|<bg:inp.bsn st:exact="true">565898785</bg:inp.bsn>|, 'Proper search for bsn');
        like($xml, qr|<bg:voorvoegselGeslachtsnaam st:exact="false">De</bg:voorvoegselGeslachtsnaam>|, 'Proper search for voorvoegselGeslachtsnaam');
        like($xml, qr|<bg:geslachtsnaam st:exact="false">Fue</bg:geslachtsnaam>|, 'Proper search for geslachtsnaam');
        like($xml, qr|<bg:geslachtsaanduiding st:exact="true">M</bg:geslachtsaanduiding>|, 'Proper search for geslachtsaanduiding');
        like($xml, qr|<bg:geboortedatum st:exact="true">19830405</bg:geboortedatum>|, 'Proper search for geboortedatum');

        $self->_validate_xml('npslv01', $xml);
    }

    {   ### Various addressdata
        my $xml         = $processor->search_np(
            {
                personal_number                     => '565898785',
                prefix                              => 'De',
                family_name                         => 'Fue',
                gender                              => 'M',
                date_of_birth                       => '19830405',
                'address_residence.street'          => 'Donker Curtiusstraat',
                'address_residence.street_number'   => '521',
            }
        );

        like($xml, $xml_tests->{$_}, 'Found proper info in XML for "' . $_ . '"') for keys %$xml_tests;
        like($xml, qr|<bg:inp.bsn st:exact="true">565898785</bg:inp.bsn>|, 'Proper search for bsn');
        like($xml, qr|<bg:voorvoegselGeslachtsnaam st:exact="false">De</bg:voorvoegselGeslachtsnaam>|, 'Proper search for voorvoegselGeslachtsnaam');
        like($xml, qr|<bg:geslachtsnaam st:exact="false">Fue</bg:geslachtsnaam>|, 'Proper search for geslachtsnaam');
        like($xml, qr|<bg:geslachtsaanduiding st:exact="true">M</bg:geslachtsaanduiding>|, 'Proper search for geslachtsaanduiding');
        like($xml, qr|<bg:geboortedatum st:exact="true">19830405</bg:geboortedatum>|, 'Proper search for geboortedatum');

        $self->_validate_xml('npslv01', $xml);
    }
}

sub processor_bg0310_usage_parse_np : Tests {
    my $self    = shift;

    my $xml     = $zs->slurp(@{ XML_PATH() }, '101-prs-create-tinus.xml');

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852',
        interface_uuid => 'c516573e-7522-475e-96a6-3a7b2442ff20',
    );

    my $result      = $processor->parse_np($xml);
}

sub processor_bg0310_usage_import_np : Tests {
    my $self        = shift;

    my $subject     = {
        'personal_number'           => '123456789',
        'personal_number_a'         => '1987654321',
        'initials'                  => 'D.',
        'first_names'               => 'Don',
        'family_name'               => 'Fuego',
        'surname'                   => 'The Fuego',
        'gender'                    => 'M',
        'place_of_birth'            => 'Amsterdam',
        'date_of_birth'             => '1982-06-05',
        'use_of_name'               => 'E',
        'address_residence'         => {
            street                  => 'Muiderstraat',
            street_number           => 42,
            street_number_letter    => 'a',
            street_number_suffix    => '521',
            city            => 'Amsterdam',
            zipcode         => '1011PZ',
            country         => {
                label           => 'Nederland',
                dutch_code      => 6030,
            }
        },
        'address_correspondence'         => {
            street                  => 'Donkerstraat',
            street_number           => 1,
            street_number_letter    => 'b',
            street_number_suffix    => '522',
            city            => 'Donkerdam',
            zipcode         => '1011PA',
            country         => {
                label           => 'Nederland',
                dutch_code      => 6030,
            }
        },
        partner                     => {
            'personal_number'           => '123456789',
            'personal_number_a'         => '1987654321',
            'family_name'               => 'Fuego',
            'prefix'                    => 'The',
        }
    };

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );

    ### Import with complete subject
    {
        my $xml         = $processor->import_np(
            {
                subject             => $subject,
                internal_identifier => 555,
            }
        );

        my $xml_tests = {
            'inp.bsn'       => qr|<bg:inp.bsn>123456789</bg:inp.bsn>|,
            'berichtcode'   => qr|<st:berichtcode>Lk01</st:berichtcode>|,
            'tijdstipbericht' => qr|<st:tijdstipBericht>\d{17}</st:tijdstipBericht>|,
            'referentienummer' => qr|<st:referentienummer>565487852</st:referentienummer>|,
            'sleutelVerzendend' => qr|st:sleutelVerzendend="555"|,
        };

        like($xml, $xml_tests->{$_}, 'Found proper info in XML for "' . $_ . '"') for keys %$xml_tests;

        $self->_validate_xml('npslk01', $xml);
    }

    ### Import with external_identifier (do not send subject data)
    {
        my $xml         = $processor->import_np(
            {
                subject             => $subject,
                internal_identifier => 555,
                external_identifier => 999,
            }
        );

        my $xml_tests = {
            'berichtcode'   => qr|<st:berichtcode>Lk01</st:berichtcode>|,
            'tijdstipbericht' => qr|<st:tijdstipBericht>\d{17}</st:tijdstipBericht>|,
            'referentienummer' => qr|<st:referentienummer>565487852</st:referentienummer>|,
            'sleutelVerzendend' => qr|st:sleutelVerzendend="555"|,
            'sleutelOntvangend' => qr|st:sleutelOntvangend="999"|,
        };

        unlike($xml, qr/inp\.bsn/, 'No content send');
        like($xml, $xml_tests->{$_}, 'Found proper info in XML for "' . $_ . '"') for keys %$xml_tests;

        $self->_validate_xml('npslk01', $xml);
    }

}


=head1 IMPLEMENTATION TETSS

=head2 _map_params

=cut

sub processor_bg0310__map_params : Tests {
    my $self        = shift;

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );

    my $mapping = {
        personal_number => 'inp.bsn',
        family_name     => 'geslachtsnaam',
    };

    my $to_stuf = $processor->_map_params({ personal_number => 545687897, family_name => 'Fuego'}, $mapping);
    cmp_deeply($to_stuf, [ { name => 'inp.bsn', value => 545687897 }, { name => 'geslachtsnaam', value => 'Fuego'} ], 'Mapped values to StUF values');
}

sub processor_bg0310__apply_search_settings : Tests {
    my $self        = shift;

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );

    throws_ok(
        sub {
            $processor->_apply_search_settings([]);
        },
        qr/for second parameter "object_type"/,
        'Exception: missing object_type'
    );

    throws_ok(
        sub {
            $processor->_apply_search_settings([], 'ASD');
        },
        qr/for second parameter "object_type"/,
        'Exception: invalid object_type'
    );

    my $to_stuf = $processor->_apply_search_settings(
        [
            {
                name => 'inp.bsn',
                value => 545687897,
            },
            {
                name => 'geslachtsnaam',
                value =>'Fuego'
            }
        ],
        'NPS'
    );

    cmp_deeply(
        $to_stuf,
        [
            {
                'name'  => 'inp.bsn',
                'exact' => 'true',
                'value' => '545687897'
            },
            {
                'name'  => 'geslachtsnaam',
                'exact' => 'false',
                'value' => 'Fuego'
            },
        ],
        'Mapped values to StUF values'
    );
}

sub processor_search_to_string : Tests {
    my $self    = shift;

    my $vars    = {
        'personal_number'           => '123456789',
        'personal_number_a'         => '1987654321',
        'initials'                  => 'D.',
        'first_names'               => 'Don',
        'family_name'               => 'Fuego',
        'prefix'                    => 'The',
        'gender'                    => 'M',
        'date_of_birth'             => '1982-06-05',
        'use_of_name'               => 'N',
        'address_residence.street'                  => 'Muiderstraat',
        'address_residence.street_number'           => 42,
        'address_residence.street_number_letter'    => 'a',
        'address_residence.street_number_suffix'    => '521',
        'address_residence.city'                    => 'Amsterdam',
        'address_residence.zipcode'                 => '1011PZ',
    };

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );

    is($processor->search_to_string($vars), '123456789, The, Fuego', "Found correct readable string");
}

sub processor__gen_stuf3_structure : Tests {
    my $self    = shift;

    my $vars    = [
        {
            name    => 'bg:inp.bsn',
            value   => '123456789',
        },
        {
            name    => 'bg:voorletters',
            value   => 'M',
        },
        {
            group               => 'bg:inp.heeftAlsEchtgenootPartner',
            entiteittype        => 'NPSNPSHUW',
            verwerkingssoort    => 'T',
            values  => [
                {
                    group               => 'gerelateerde',
                    entiteittype        => 'NPSNPSHUW',
                    verwerkingssoort    => 'T',
                    values              => [
                        {
                            name    => 'bg:inp.bsn',
                            value   => '123456789',
                        },
                        {
                            name    => 'bg:voorletters',
                            value   => 'M',
                        },
                    ]
                }
            ],
        },
        {
            group   => 'bg:verblijfsadres',
            values  => [
                {
                    name    => 'bg:wpl.woonplaatsNaam',
                    value   => 'Amsterdam',
                },
                {
                    name    => 'bg:gor.openbareRuimteNaam',
                    value   => 'Donker',
                },
                {
                    name    => 'aoa.huisnummer',
                    value   => '32',
                }
            ]
        }
    ];

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );

    ok($processor->_gen_stuf3_structure($vars), 'Got a plaintext structure');
}

sub processor__generate_stuf3_schema_from_entity : Tests {
    my $self        = shift;

    my $subject     = {
        'personal_number'           => '123456789',
        'personal_number_a'         => '1987654321',
        'initials'                  => 'D.',
        'first_names'               => 'Don',
        'family_name'               => 'Fuego',
        'surname'                   => 'The Fuego',
        'prefix'                    => 'The',
        'gender'                    => 'M',
        'place_of_birth'            => 'Amsterdam',
        'date_of_birth'             => '1982-06-05',
        'use_of_name'               => 'E',
        'address_residence'         => {
            street                  => 'Muiderstraat',
            street_number           => 42,
            street_number_letter    => 'a',
            street_number_suffix    => '521',
            city            => 'Amsterdam',
            zipcode         => '1011PZ',
            country         => {
                label           => 'Nederland',
                dutch_code      => 6030,
            }
        },
        'address_correspondence'         => {
            street                  => 'Donkerstraat',
            street_number           => 1,
            street_number_letter    => 'b',
            street_number_suffix    => '522',
            city            => 'Donkerdam',
            zipcode         => '1011PA',
            country         => {
                label           => 'Nederland',
                dutch_code      => 6030,
            }
        },
        partner                     => {
            'personal_number'           => '123456789',
            'personal_number_a'         => '1987654321',
            'family_name'               => 'Fuego',
            'prefix'                    => 'The',
        }
    };

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );

    my $schema = $processor->_generate_stuf3_schema_from_entity($subject);

    my $xml = $processor->_gen_stuf3_structure($schema);
    like($xml, qr|<bg:inp.bsn>123456789</bg:inp.bsn>|, 'Got bsn in xml');
    like($xml, qr|<bg:verblijfsadres>|, 'Got block "verblijfsadres" in xml');
    like($xml, qr|<bg:gerelateerde st:entiteittype="NPS" st:verwerkingssoort="T">|, 'Got block "gerelateerde" in xml');

}

sub processor_escape_value : Tests {
    my $self        = shift;

    my $processor   = Zaaksysteem::StUF::0301::Processor::BG0310->new(
        schema => $schema,
        reference_id => '565487852'
    );

    is ($processor->escape_value('< this "is" a & \'test\'>'), '&lt; this &quot;is&quot; a &amp; &apos;test&apos;&gt;', 'Correctly filtered text');
}

=head1 PRIVATE TEST METHODS

=head2 _validate_xml

    $ENV{XML_COMPILE_TESTS} = 1;

    $self->_validate_xml($method, $STRING_XML);

Validate the XML via L<XML::Compile>

=cut

sub _validate_xml {
    my $self        = shift;
    my $method      = shift;
    my $xml         = shift;

    SKIP: {
        skip "Set ENV: XML_COMPILE_TESTS=1 if you would like to validate XML via XML::Compile", 1 unless $ENV{XML_COMPILE_TESTS};

        unless ($self->{_parser}) {
            my $compile = Zaaksysteem::XML::Compile->xml_compile;

            $compile->add_class('Zaaksysteem::StUF::0301::Instance');

            $self->{_parser} = $compile;
        }

        lives_ok(
            sub { $self->{_parser}->stuf0301->$method('READER', $xml) },
            'Validated XML via XML::Compile'
        );
    };
}


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
