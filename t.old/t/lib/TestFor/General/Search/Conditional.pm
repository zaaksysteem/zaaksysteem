package TestFor::General::Search::Conditional;
use base qw(Test::Class);

use TestSetup;

use Test::DummySearchTerm;
use Zaaksysteem::Search::Term::Column;
use Zaaksysteem::Search::Term::Literal;
use Zaaksysteem::Search::Conditional;

use Moose::Util qw(apply_all_roles);

sub test_base_conditional : Tests {
    my @TERM1;
    my $term1 = Test::DummySearchTerm->new(source => \@TERM1);

    my @TERM2;
    my $term2 = Test::DummySearchTerm->new(source => \@TERM2);

    my $cond = Zaaksysteem::Search::Conditional->new(
        lterm    => $term1,
        rterm    => $term2,
        operator => '=',
    );

    @TERM1 = (qw(aap noot mies));
    @TERM2 = (qw(foo bar baz));
    my $rv = $cond->evaluate();

    is_deeply(
        $rv,
        \[ "(aap = foo)", qw(noot mies bar baz) ],
        'Evaluation a condition with two terms returns the correct value',
    );

    @TERM1 = (qw(aap));
    @TERM2 = (qw(foo bar baz));
    $rv = $cond->evaluate();

    is_deeply(
        $rv,
        \[ "(aap = foo)", qw(bar baz) ],
        'Evaluation a condition with two terms (1st without placeholders) returns the correct value',
    );

    @TERM1 = (qw(aap noot mies));
    @TERM2 = (qw(foo));
    $rv = $cond->evaluate();

    is_deeply(
        $rv,
        \[ "(aap = foo)", qw(noot mies) ],
        'Evaluation a condition with two terms (2nd without placeholders) returns the correct value',
    );
}

sub test_contains_conditional : Tests {
    my $mock_rs = Test::MockObject->new();
    $mock_rs->mock(
        'map_hstore_key',
        sub {
            my $self = shift;
            return "hstore[$_[0]]";
        }
    );

    my $lterm = Zaaksysteem::Search::Term::Column->new(value => 'left_col');
    my $rterm = Zaaksysteem::Search::Term::Literal->new(value => 'right_lit');

    my $cond = Zaaksysteem::Search::Conditional->new(
        lterm    => $lterm,
        rterm    => $rterm,
        operator => 'contains',
    );

    my $rv = $cond->evaluate($mock_rs);
    is_deeply(
        $rv,
        \[
            "(string_to_array(hstore[left_col], '\x1E') @> ?)",
            [ {} => [ 'right_lit' ] ],
        ],
        "'contains' conditional evaluated to the correct DBIx::Class arguments."
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
