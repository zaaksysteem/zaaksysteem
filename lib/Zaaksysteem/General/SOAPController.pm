package Zaaksysteem::General::SOAPController {

    use warnings;
    use strict;
    use base 'Catalyst::Controller::SOAP';

    sub begin : Private {
        my $self = shift;
        my $c = shift;

        # SOAP responses are always UTF-8-encoded.
        # Catalyst::Controller::SOAP sets a Content-Type 'text/xml; charset=UTF-8' header,
        # and puts a byte string in $c->body. This was correct in older
        # versions of Catalyst.
        #
        # Catalyst::Response sees the "text/xml" assumes it *also* needs to
        # encode the body. By forcing Catalyst's idea of the current encoding to something
        # other than the SOAP context, this smart encoding is turned off.
        #
        # Catalyst sets 'binmode' on STDERR to the selected encoding too, so
        # we undo that as well.
        $c->encoding('US-ASCII');
        binmode(STDERR);

        $c->forward('/page/begin');
    }

    sub _parse_SOAP_attr {
        my $self        = shift;

        my %actionattrs = $self->next::method(@_);

        ### ActionClass code
        if ($actionattrs{ActionClass} !~ /^Catalyst/) {
            $actionattrs{ActionClass} = '+Catalyst::Action::'
                . $actionattrs{ActionClass};
        }

        return %actionattrs;

    }
};

1;

=head1 NAME

Zaaksysteem::General::SOAPController -  Wrapper around Catalyst::Controller::SOAP

=head1 DESCRIPTION

The only purpose of this module is making sure the ActionClass is properly
prefixed. Somehow, this has changed in a future version of Catalyst.

Remove the ActionClass code when we reach "the" future version of Catalyst

=cut


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

