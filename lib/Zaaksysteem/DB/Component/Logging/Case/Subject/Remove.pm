package Zaaksysteem::DB::Component::Logging::Case::Subject::Remove;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head2 onderwerp

Generate a human-readable summary for the removal of a zaak-betrokkene.

=cut

sub onderwerp {
    my $self = shift;

    sprintf('Betrokkene "%s" verwijderd van zaak %s als %s',
        $self->data->{ subject_name },
        $self->data->{ case_id },
        $self->data->{ role }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
