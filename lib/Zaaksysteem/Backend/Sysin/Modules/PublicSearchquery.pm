package Zaaksysteem::Backend::Sysin::Modules::PublicSearchquery;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::PublicSearchquery - Interface to configure a public searchquery in zaaksysteem

=head1 DESCRIPTION

This module allows the configuration of a public search page on zaaksysteem.nl

=cut

use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;

use constant INTERFACE_ID => 'publicsearchquery';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_title',
        type        => 'text',
        label       => 'Titel',
        required    => 1,
        description => 'Geef een titel voor deze zoekopdracht.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_user',
        type     => 'spot-enlighter',
        label    => 'Medewerker',
        required => 1,
        description => 'Bepaal hier welke medewerkerrechten de zoekopdracht krijgt.',
        data => {
            restrict    => 'contact/medewerker',
            placeholder => 'Type uw zoekterm',
            label       => 'naam',
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_query',
        type => 'spot-enlighter',
        label => 'Zoekopdracht',
        required => 0,
        description => 'Door een hier een opgeslagen zoekopdracht te specificeren kan bepaald worden welke zaken de externe partij kan zien en/of gebruiken',
        data => {
            restrict => 'objects',
            placeholder => 'Type uw zoekterm',
            label => 'label',
            params => { object_type => 'saved_search' }
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_uri',
        type => 'display',
        label => 'API URI',
        description => 'De genoteerde URI is de locatie waar de externe partij met het Zaaksysteem kan communiceren',
        required => 0,
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Publieke zoekopdracht',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list => [
    ],
    trigger_definition => {
    },
    test_interface  => 0,
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 METHODS

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    $form->load_values({
        interface_api_uri => $opts->{ base_url } . 'form/public_search/' . $opts->{entry}->id
    });

    return $form;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
