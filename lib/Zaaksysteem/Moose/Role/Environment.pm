package Zaaksysteem::Moose::Role::Environment;
use Moose::Role;

use Zaaksysteem::Types qw(SecureHTTP_URI);
require Zaaksysteem::Version;

=head1 NAME

Zaaksysteem::Moose::Role::Environment - A role that implements some environment things

=head1 DESCRIPTION

=head1 SYSNOPSIS

    package Foo;
    use Zaaksysteem::Moose;

    with qw(Zaaksysteem::Moose::Role::Environment);

=head1 ATTRIBUTES

=head2 environment

The Zaaksysteem environment (hostname), this corresponds to in most (if not
all) cases to C<< $c->config->{gemeente_id} >>. Has the predicate C<has_environment>.

=head2 version

The Zaaksysteem version, this has the default $VERSION which is found in
L<Zaaksysteem::Version>.

=head2 base_uri

The URI of the environment, this corresponds to in most cases to C<<
$c->uri_for('/') >>. Has the predicate C<has_base_uri>.

=head2 request_id

The request ID of the given action, this corresponds to in most (if not all)
cases to C<< $c->get_session_id >>. Has the predicate C<has_request_id>.

=cut

has environment => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_environment',
);

has version => (
    is        => 'ro',
    isa       => 'Str',
    default   => sub { return $Zaaksysteem::Version::VERSION // '0' },
);

has base_uri => (
    is        => 'ro',
    isa       => SecureHTTP_URI,
    predicate => 'has_base_uri',
    coerce    => 1,
);

has request_id => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_request_id',
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
