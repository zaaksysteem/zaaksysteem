package Zaaksysteem::Model::Template::Base;

use Moose;

has document => (is => 'ro');

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig({ document => shift });
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

