package Zaaksysteem::Roles::Session;
use Moose::Role;
use Digest::SHA qw(sha1_base64);

requires qw(type);

=head1 NAME

Zaaksysteem::Roles::Session - A session role for Zaaksysteem

=cut

has redis => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Store::Redis',
    required => 1,
);

has cookiejar => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Store::Redis',
    required => 1,
);

has expire => (
    is       => 'ro',
    isa      => 'Int',
    default  => 10800,
);

sub _generate_redis_id {
    my ($self, $id) = @_;
    # Just obfuscation, not security, as this is used as a key. Salted hashes
    # would make the lookup a bit more complex.
    return join(':', $self->type, sha1_base64($id));
}

=head2 start_session($token, $session_id)

Start retrievable session

=cut

sub start_session {
    my ($self, $id, $session_id) = @_;

    my $redis_id = $self->_generate_redis_id($id);

    $self->redis->set($redis_id, "session:$session_id");
    $self->redis->expire($redis_id, $self->expire);
    return 1;
}

=head2 get_session($token)

Get a retrievable session, returns the session ID

    my ($redis_id, $sid) = $self->get_session($token);
    $c->session($c->session($c->get_session_data($sid));

=cut

sub get_session {
    my ($self, $id) = @_;

    my $redis_id = $self->_generate_redis_id($id);
    my $session_id = $self->redis->get($redis_id);

    return unless defined $session_id;
    return ($redis_id, $session_id);
}

=head2 confirm_session($token)

Confirm that the session exists and returns the session ID. This will remove
the retrievable session

=cut

sub confirm_session {
    my ($self, $id) = @_;

    my ($redis_id, $session_id) = $self->get_session($id);
    return unless defined $session_id;

    $self->redis->del($redis_id);
    return $session_id;
}

=head2 delete_cookiejar_session($session_id)

Delete the session from the cookiejar. This is more than likely to be the
catalyst session.

=cut

sub delete_cookiejar_session {
    my ($self, $session_id) = @_;
    $self->cookiejar->del($session_id);
    $self->cookiejar->del("json:$session_id");
    return 1;
}

=head2 end_session($token, $optional_coderef)

Confirms and deletes both the retrievable session and the cookiejar session.
See also C<confirm_session> and C<delete_cookiejar_session>. You can also pass
in a code ref, that will be handle some logic between confirming and deleting
the session.

    my $bool = $self->end_session($token); # 0 or 1 is the session existed

    my $book = $self->end_session(
        $token,
        sub {
            my $sid = shift;    # Session id
            $c->session($c->get_session_data($sid));
        }
    );

=cut

sub end_session {
    my ($self, $id, $sub) = @_;

    my $session_id = $self->confirm_session($id);
    return 0 unless defined $session_id;

    if (ref $sub eq 'CODE') {
        $sub->($session_id);
    }

    $self->delete_cookiejar_session($session_id);
    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
