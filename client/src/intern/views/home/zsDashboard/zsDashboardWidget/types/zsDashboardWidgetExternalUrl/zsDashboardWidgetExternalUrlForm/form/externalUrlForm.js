// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const VALID_URL_REGEXP = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;

const validUrl = (url) => {
  return VALID_URL_REGEXP.test(url);
};

export default () => {
  return {
    getDefaults: () => {
      return {};
    },
    processChange: (name, value, values) => {
      let vals = values;

      return vals.merge({ [name]: value });
    },
    fields: () => {
      return [
        {
          name: 'title',
          label: 'Titel',
          template: 'text',
          required: true,
        },
        {
          name: 'url',
          label: 'URL',
          template: 'text',
          required: true,
          valid: ['$values', (values) => Boolean(validUrl(values.url))],
        },
      ];
    },
  };
};
