// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isArray from 'lodash/isArray';

const conversions = {
  '!=': '!',
  '==': '=',
  '<=': '[',
  '>=': ']',
};

const symbols = {
  '^': { infix: '_POW' },
  '*': { infix: '_MUL' },
  '/': { infix: '_DIV' },
  '%': { infix: '_MOD' },
  '+': { infix: '_ADD', prefix: '_POS' },
  '-': { infix: '_SUB', prefix: '_NEG' },
  ']': { infix: '_EGT' },
  '>': { infix: '_GT' },
  '<': { infix: '_LT' },
  '[': { infix: '_ELT' },
  '=': { infix: '_EQ' },
  '!': { infix: '_NEQ' },
};

const operators = {
  _POW: {
    name: 'Power',
    precedence: 4,
    associativity: 'right',
    method: (x, y) => Math.pow(x, y),
  },
  _POS: {
    name: 'Positive',
    precedence: 3,
    associativity: 'right',
    method: (x) => x,
  },
  _NEG: {
    name: 'Negative',
    precedence: 3,
    associativity: 'right',
    method: (x) => -x,
  },
  _MUL: {
    name: 'Multiply',
    precedence: 2,
    associativity: 'left',
    method: (x, y) => x * y,
  },
  _DIV: {
    name: 'Divide',
    precedence: 2,
    associativity: 'left',
    method: (x, y) => x / y,
  },
  _MOD: {
    name: 'Modulo',
    precedence: 2,
    associativity: 'left',
    method: (x, y) => x % y,
  },
  _ADD: {
    name: 'Add',
    precedence: 1,
    associativity: 'left',
    method: (x, y) => x + y,
  },
  _SUB: {
    name: 'Subtract',
    precedence: 1,
    associativity: 'left',
    method: (x, y) => x - y,
  },
  _GT: {
    name: 'Greater than',
    precedence: 0,
    associativity: 'left',
    method: (x, y) => x > y,
  },
  _LT: {
    name: 'Lesser than',
    precedence: 0,
    associativity: 'left',
    method: (x, y) => x < y,
  },
  _EGT: {
    name: 'Equal to or greater than',
    precedence: 0,
    associativity: 'left',
    method: (x, y) => x >= y,
  },
  _ELT: {
    name: 'Equal to or lesser than',
    precedence: 0,
    associativity: 'left',
    method: (x, y) => x <= y,
  },
  _EQ: {
    name: 'Equal',
    precedence: 0,
    associativity: 'left',
    method: (x, y) => x == y,
  },
  _NEQ: {
    name: 'Not equal',
    precedence: 0,
    associativity: 'left',
    method: (x, y) => x != y,
  },
};

const isSymbol = (token) => Object.keys(symbols).includes(token);
const isOperator = (token) => Object.keys(operators).includes(token);
const isNumber = (token) => /(\d+\.\d*)|(\d*\.\d+)|(\d+)/.test(token);
const isOpenParenthesis = (token) => /\(/.test(token);
const isCloseParenthesis = (token) => /\)/.test(token);
const isComma = (token) => /,/.test(token);
const isWhitespace = (token) => /\s/.test(token);

const round = (number, precision) => {
  const modifier = Math.pow(10, precision);

  return !modifier
    ? Math.round(number)
    : Math.round(number * modifier) / modifier;
};

function topOperatorHasPrecedence(operatorStack, currentOperatorName) {
  if (!operatorStack.length) return false;

  const topToken = operatorStack[operatorStack.length - 1];

  if (!isOperator(topToken)) return false;

  const topOperator = operators[topToken];
  const currentOperator = operators[currentOperatorName];

  if (currentOperator.method.length === 1 && topOperator.method.length > 1)
    return false;

  if (topOperator.precedence > currentOperator.precedence) return true;

  return (
    topOperator.precedence === currentOperator.precedence &&
    topOperator.associativity === 'left'
  );
}

function determineOperator(token, previousToken) {
  if (
    previousToken === undefined ||
    isOpenParenthesis(previousToken) ||
    isSymbol(previousToken) ||
    isComma(previousToken)
  ) {
    return symbols[token].prefix;
  }

  if (isCloseParenthesis(previousToken) || isNumber(previousToken)) {
    return symbols[token].infix;
  }

  return undefined;
}

function parse(expression) {
  if (!expression.length) {
    throw Error('No input');
  }

  const pattern = /(\d+\.\d*)|(\d*\.\d+)|(\d+)|([a-zA-Z0-9_]+)|(.)/g;

  const infixExpression = (expression.match(pattern) || [])
    .filter((token) => !isWhitespace(token))
    .map((token) => token.toUpperCase());

  return infixExpression;
}

function convert(infixExpression) {
  if (!infixExpression.length) {
    throw Error('No valid tokens');
  }

  const operatorStack = [];
  const arityStack = [];
  const postfixExpression = [];
  let methodIsNewlyDeclared = false;

  infixExpression.forEach((token, index) => {
    if (methodIsNewlyDeclared && !isOpenParenthesis(token)) {
      throw Error(`Misused method: ${operatorStack[operatorStack.length - 1]}`);
    }

    if (isNumber(token)) {
      postfixExpression.push(parseFloat(token));
      return;
    }

    if (isSymbol(token)) {
      const operatorName = determineOperator(token, infixExpression[index - 1]);
      const operator = operators[operatorName];

      if (operator === undefined) {
        throw Error(`Misused operator: ${token}`);
      }

      while (topOperatorHasPrecedence(operatorStack, operatorName)) {
        postfixExpression.push(operatorStack.pop());
      }

      operatorStack.push(operatorName);
      return;
    }

    if (isOpenParenthesis(token)) {
      operatorStack.push(token);
      return;
    }

    if (isComma(token)) {
      arityStack[arityStack.length - 1] += 1;

      while (!isOpenParenthesis(operatorStack[operatorStack.length - 1])) {
        if (!operatorStack.length) {
          throw Error('Invalid token: ,');
        }

        postfixExpression.push(operatorStack.pop());
      }

      return;
    }

    if (isCloseParenthesis(token)) {
      while (!isOpenParenthesis(operatorStack[operatorStack.length - 1])) {
        if (!operatorStack.length) {
          throw Error('Mismatched parentheses');
        }

        postfixExpression.push(operatorStack.pop());
      }

      operatorStack.pop();

      return;
    }

    throw Error(`Invalid token: ${token}`);
  });

  while (operatorStack.length) {
    const operator = operatorStack[operatorStack.length - 1];

    if (isOpenParenthesis(operator) || isCloseParenthesis(operator)) {
      throw Error('Mismatched parentheses');
    }

    postfixExpression.push(operatorStack.pop());
  }

  return postfixExpression;
}

function resolve(postfixExpression) {
  if (!postfixExpression.length) {
    throw Error('No operations');
  }

  const evaluationStack = [];

  postfixExpression.forEach((token) => {
    if (isNumber(token)) {
      evaluationStack.push(token);
      return;
    }

    const operator = operators[token];

    if (evaluationStack.length < operator.method.length) {
      throw Error(`Insufficient operands for operator: ${operator.name}`);
    }

    if (token === '_DIV' && evaluationStack[1] === 0) {
      throw Error('Division by zero');
    }

    const result = operator.method(
      ...evaluationStack.splice(-operator.method.length)
    );
    evaluationStack.push(result);
  });

  if (evaluationStack.length > 1) {
    throw Error('Insufficient operators');
  }

  const reduction = evaluationStack[0];
  const result = round(reduction, 8);

  return result;
}

function evaluateMathString(rawExpression) {
  let expression = rawExpression;
  Object.keys(conversions).forEach((k) => {
    expression = expression.replace(k, conversions[k]);
  });
  expression = parse(expression.replace(/(:pi|constant\("pi"\))/g, Math.PI));
  const postfixExpression = convert(expression);
  const result = resolve(postfixExpression);
  return result;
}

export default function evaluateFormula(formula, values) {
  let val;

  const expression = formula.replace(
    /([a-zA-Z]+(?:[a-zA-Z0-9_.]+))/g,
    (match, key) => {
      let value,
        name = key;

      if (name.indexOf('.') === -1) {
        name = `attribute.${name}`;
      }

      value = values[name];

      if (isArray(value)) {
        value = value[0];
      }

      value = String(value).replace(',', '.');

      return isNaN(value) ||
        value === undefined ||
        value === null ||
        value === ''
        ? 0
        : Number(value);
    }
  );

  try {
    val = evaluateMathString(expression);

    if (
      isNaN(val) ||
      val === Number.POSITIVE_INFINITY ||
      val === Number.NEGATIVE_INFINITY
    ) {
      val = null;
    } else {
      val = Math.round(val * 100) / 100;
    }
  } catch (e) {
    console.error(e);
    val = null;
  }

  return val;
}
