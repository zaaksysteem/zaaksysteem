// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default function shiftMonth(referenceDate, offset) {
  const date = new Date(Number(referenceDate));
  const day = date.getDate();

  date.setMonth(date.getMonth() + offset);

  if (date.getDate() !== day) {
    date.setDate(0);
  }

  return date;
}
