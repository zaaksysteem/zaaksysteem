// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseAction', [
    '$timeout',
    function ($timeout) {
      return {
        controller: [
          function ($scope) {
            var ctrl = this;

            ctrl.loading = false;

            ctrl.setLoading = function (loading) {
              ctrl.loading = loading;
            };

            ctrl.isLoading = function () {
              return ctrl.loading;
            };
          },
        ],
        controllerAs: 'caseAction',
      };
    },
  ]);
})();
