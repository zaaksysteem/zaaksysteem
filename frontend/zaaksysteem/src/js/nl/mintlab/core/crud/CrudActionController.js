// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem')
    .controller('nl.mintlab.core.crud.CrudActionController', [
      '$scope',
      '$interpolate',
      function ($scope, $interpolate) {
        function getUrlParameters() {
          var params = $scope.action.data ? $scope.action.data.params : null;

          if (!params) {
            params = {};
          }

          if (_.isString(params)) {
            params = $interpolate(params)($scope);
            try {
              params = JSON.parse(params);
            } catch (exception) {}
          }

          return params;
        }

        $scope.getEzraDialogUrl = function () {
          var data = $scope.action.data || {},
            url = $interpolate(data.url, false)($scope);

          return url;
        };

        $scope.getEzraDialogParams = function () {
          var options = _.clone($scope.action.data);
          options.cgi_params = getUrlParameters();
          return options;
        };

        $scope.getActionUrl = function (action) {
          var url = action.data ? action.data.url : '';
          return $interpolate(url)($scope);
        };
      },
    ]);
})();
