// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  'use strict';

  angular.module('Zaaksysteem.admin', [
    'Zaaksysteem.admin.casetype',
    'Zaaksysteem.admin.objecttype',
    'Zaaksysteem.admin.instances',
  ]);
})();
