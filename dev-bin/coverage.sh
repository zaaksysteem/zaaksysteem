#!/bin/bash

if [ -e .mytest ] ; then
    ./zs_prove t/9*.t
fi

base=cover_db
[ -e $base ] && cover -delete

HARNESS_PERL_SWITCHES=-MDevel::Cover=+select,^t/inc/.*\.pm,+ignore,^t/.+\.t,+ignore,^lib/Zaaksysteem/Schema.+ ./zs_prove $@
unset PERL5LIB

cover --report html_basic

report=$base/coverage.html

if [ -e "$report" ] ; then
    mv $report $base/index.html
    chmod 755 $base
    # We only want the HTML files.. the rest not needed.
    rm -rf $base/{runs,structure,cover.13}
fi
rm -f *.err
